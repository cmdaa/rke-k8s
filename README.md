## Install a local Kubernetes cluster using rke
Installing a local Kubernetes (k8s) cluster using
[rke](https://rancher.com/docs/rke/latest/en/).

1.  Create and enter a directory to hold the rke configuration (*NOTE: This
    *COULD* be the directory containing this repository*):
    ```bash
    mkdir ~/rke
    cd ~/rke
    ```
2.  Retrieve and install rke 1.3 for access to k8s 1.23:
    ```bash
    curl -Lk -o rke_linux-v1.3.12-amd64 \
        https://github.com/rancher/rke/releases/download/v1.3.12/rke_linux-amd64
    # ...

    # ASSUMING ~/.local/bin is in your `PATH`:
    install rke_linux-v1.3.12-amd64 ~/.local/bin/rke
    # ...

    rke version
    # INFO[0000] Running RKE version: v1.3.12
    # ...

    # Show the available versions of k8s that may be installed
    rke config -l -a
    # v1.22.10-rancher1-1
    # v1.23.7-rancher1-1
    # v1.21.13-rancher1-1
    # v1.18.20-rancher1-3
    # v1.20.15-rancher1-4
    # v1.19.16-rancher1-6
    ```
3.  Generate a self-signed CA:
    ```bash
    # Generate the CA passing the organization name you wish to use.
    #
    # This will generate a CA/ subdirectory and populate it with a CA private
    # key (CA/private/ca.key.pem), self-signed CA certificate
    # (CA/certs/ca.cert.pem), and configuration (CA/openssl.cnf) to allow
    # signing Certificate Signing Requests (CSR).
    #
    ./generate-ca cmdaa
    # ...
    ```
4.  <a name='step4'></a>Generate a wildcard certificate/key for the top-level
    domain that is to be associated with the cluster (e.g. *.test):
    ```bash
    # Generate a wildcard certificate/key for the top-level domain you wish to
    # use for your cluster (test => *.test)
    #
    # This will generate a private key (CA/private/wild.%dn%.key.pem), CSR for
    # the wildcard certificate (CA/csr/wild.%dn%.csr.pem), and a signed
    # wildcard certificate (CA/certs/wild.%dn%.cert.pem)
    #
    # It will then use the private key and signed certificate to generate a k8s
    # secret (secret-ingress-default-cert.yaml) that we will use in a following
    # step.
    #
    ./generate-ingress-cert test
    # ...
    ```
5.  Configure a new k8s cluster:
    ```bash
    # Accept defaults for everything except the options below, choosing the
    # path to YOUR ssh private key and your USER name ($USER):
    rke config

    # ...
    # [+] SSH Address of host (1) [none]: localhost
    # [+] SSH Private Key Path of host (localhost) [none]: ~/.ssh/id_rsa
    # ...
    # [+] SSH User of host (localhost) [ubuntu]: USER
    # ...
    # [+] Is host (localhost) a Worker host (y/n)? [n]: y
    # [+] Is host (localhost) an etcd host (y/n)? [n]: y
    # ...
    ```

6.  Update the generated `cluster.yaml` to make use of the wildcard
    certificate/key. The following will include the contents of
    `secret-ingress-default-cert.yaml` (generated via `./generate-ingress-cert`
    above) in the `addons:` section and update the `ingress:` section with
    `extra_args` identifying that secret:

    ```bash
    ed cluster.yml <<EOF
    /^addons:
    s/ ""/ |-/
    .r !sed 's/^/  /' secret-ingress-default-cert.yaml
    /^ingress:
    /^  provider:
    s/ ""/ "nginx"/
    /^  extra_args:
    s/ {}//
    .y
    .x
    s#extra_args:#  default-ssl-certificate: "ingress-nginx/ingress-default-cert"#
    wq
    EOF
    ```
7.  Bring the rke-based k8s cluster up:
    ```bash
    rke up
    # ...
    ```
8.  Save the kube config needed to access the new cluster:
    ```bash
    if [ ! -d ~/.kube ]; then mkdir ~/.kube; fi
    cp kube_config_cluster.yaml ~/.kube/config
    ```
9.  Retrieve and apply the YAML to establish a local-path volume/storage
    provisioner:
    ```bash
    # :NOTE: The curl is not required. You COULD use the URL directly in the
    #        `kubectl apply -f %url%` but we download it here so we have
    #        consistent access in the future.
    #
    curl -Lk -o local-path-storage.yaml \
      https://raw.githubusercontent.com/rancher/local-path-provisioner/v0.0.22/deploy/local-path-storage.yaml
    # ...

    # Establish the local-path provisioner
    kubectl apply -f local-path-storage.yaml
    # ...

    # Set the local-path storage class as the default
    kubectl patch storageclass local-path \
      -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
    # ...

    # View the current storage class again (to show default)
    kubectl get storageclass

    # NAME                   PROVISIONER             RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
    # local-path (default)   rancher.io/local-path   Delete          WaitForFirstConsumer   false                  97d
    ```

## Install and configure dnsmasq

If you did not use a registered DNS domain in [step 4 of Installing k8s using
rke](#step4), you will need to make use of
[dnsmasq](https://en.wikipedia.org/wiki/Dnsmasq) or equivalent in order to
access applications running within your k8s cluster.

### Ubuntu 20.04

Ubuntu makes use of
[systemd-resolved](https://manpages.ubuntu.com/manpages/focal/man8/systemd-resolved.service.8.html)
for network name resolution, including DNS.

In order to enable `dnsmasq` functionality, steps must be taken to allow
`systemd-resolved` to function normally while also recognizing `dnsmasq`. To to
this, we will use
[resolvconf](https://manpages.ubuntu.com/manpages/focal/en/man8/resolvconf.8.html) along with a bit of redirection.

```bash
# Install and setup dnsmasq and resolvconf
sudo apt install -y dnsmasq resolvconf

###
# Create a dnsmasq configuration
#
# Change `.test` below to the value used when generating the wildcard
# certificate in step 4 of Installing k8s using rke.
#
# This will redirect to the nginx ingress of the k8s cluster established above.
#
sudo tee /etc/dnsmasq.d/development.conf <<EOF
address=/.test/127.0.0.1
EOF

# Ensure the dnsmasq configuration has the proper group and mode
sudo chgrp adm /etc/dnsmasq.d/development.conf
sudo chmod 664 /etc/dnsmasq.d/development.conf

###
# Create a dnsmasq resolver configuration that we will soft-link to from
# /etc/resolv.conf
#
# Since /etc/resolv.conf will now be a soft-link, the NetworkManager will NOT
# replace it on boot.
#
sudo tee /etc/resolv-dnsmasq.conf <<EOF
nameserver 127.0.0.1
options edns0 trust-ad
EOF

sudo rm -f /etc/resolv.conf
sudo ln -s resolv-dnsmasq.conf /etc/resolv.conf

###
# Update dnsmasq to use the systemd-resolved stub resolver as its upstream
# server.
#
sudo ed /etc/dnsmasq.conf <<EOF
/^#resolv-file=
s,#resolv-file=,resolv-file=/run/systemd/resolve/stub-resolv.conf,
wq
EOF

###
# Restart dnsmasq to apply changes
#
sudo systemctl restart dnsmasq

# Enable dnsmasq so it will start on boot
sudo systemctl enable dnsmasq
```
